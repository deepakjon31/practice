class Employee(object):
	"""docstring for Employee"""
	num_of_emps = 0
	raise_amt = 1.04

	def __init__(self, first, last, pay):
		self.first = first
		self.last = last
		self.email = first +'.' + last + '@email.com'
		self.pay = pay
		Employee.num_of_emps += 1
	def fullname(self):
		return "{} {}". formate(self.first, self.last)
	def apply_raise(self):
		self.pay = int(self.pay * self,raise_amt)

	@classmethod
	def set_raise_amt(cls, amount):
		cls.raise_amt = amount
	
	@classmethod
	def from_string(cls, emp_str):
		first, last, pay = emp_str.split('-')
		return cls(first,last,pay)

	@staticmethod
	def is_workday(day):
		if day.weekday() == 5 or  day.weekday() == 6:
			return False
		return True


# emp1 = Employee('Deepak', 'kumar', 3000)
# Employee.set_raise_amt(1.05)

# print(Employee.raise_amt)
# print(emp1.raise_amt)

# emp_str1 = 'john-doe-40000'
# emp_str2 = 'pawnesh-show-5000'

# new_emp1 = Employee.from_string(emp_str1)

# #first, last, pay = emp_str1.split('-')
# #new_emp1 = Employee(first,last,pay)
# print(new_emp1.email)
# print(new_emp1.pay)


import datetime
my_date = datetime.date(2017,02,25)
print(Employee.is_workday(my_date))