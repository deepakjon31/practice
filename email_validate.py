import re
ips = ['brian-23@hackerrank.com', 'britts_54@hackerrank.com', 'lara@hackerrank.com']

for ip in ips:
    validate = re.search(r'^[\w.-]+@[a-zA-Z0-9]+\.[a-zA-Z{3}]$',ip)
    if validate:
        print("Valid Email")
    else:
        print("Not valid")