# # Binary Tree
# class BinaryTreeNode:
# 	def __init__(self, data):
# 		self.data = data
# 		self.left = None
# 		self.right = None

# 	def setData(data):
# 		self.data = data

# 	def getData(self):
# 		return self.data

# 	def getLeft(self):
# 		return self.left

# 	def getRight(self):
# 		return self.right

# #Preorder traversal

# def preordertraversal(root, result):
# 	if not root:
# 		return
# 	result.append(root.data)
# 	preordertraversal(root.left, result)
# 	preordertraversal(root.right, result)

# def  inordertraversal(root, result):
# 	if not root:
# 		return
# 	inordertraversal(root.left,result)
# 	result.append(root.data)
# 	inordertraversal(root.right, result)

def postordertraversal(root, result=[]):
	if not root:
		return
	postordertraversal(root.left,result)
	postordertraversal(root.right, result)
	result.append(root.data)
	print result


# def levelordertraversal(root, result):
# 	if root is none:
# 		return
# 	q = Queue.Queue()
# 	q.put(root)
# 	node = none
# 	while not q.empty():
# 		node = q.get()
# 		result.append(node.getData())
# 		if node.getLeft is not None:
# 			q.put(node.getLeft())
# 		if node.getRight() is not None:
# 			q.put(node.getRight())
# 	



####################### Finding the Maximum element in binary tree ###########################
from Queue import Queue
maxElement = 0
class Binary_Tree:
	def __init__(self):
		pass

	def findMaxElement(self, root):
		if root is None:
			return
		q = Queue()
		q.put(root)
		node = None
		global maxElement
		#maxElement = 0
		while not q.empty():
			node = q.get()
			if maxElement < node.getData():
				maxElement = node.getData()
			if node.left is not None:
				q.put(node.left)
			if node.right is not None:
				q.put(node.right)
print("Max Element is:", maxElement)

class BinaryTreeNode:
	def __init__(self, data):
		self.data = data
		self.left = None
		self.right = None

#print(node.getData())
if __name__ == '__main__':
	root = BinaryTreeNode(1)
	root.left = BinaryTreeNode(2)
	root.right = BinaryTreeNode(3)
	root.left.right = BinaryTreeNode(5)
	root.left.left = BinaryTreeNode(4)
	root.right.left = BinaryTreeNode(6)
	obj = Binary_Tree()
	#	obj.findMaxElement(root)
	#print "yes"
	#obj.findMaxElement(root)
	postordertraversal(root)
