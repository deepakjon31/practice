#!/usr/bin/python
#bawahaha: malodious pasword   : consonant then vowel then consonant then vowel ... alternate
#
#bwahaoha: not malodious password
import sys
import itertools

n = int(raw_input().strip())
consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z']
vowels = ['a', 'e', 'i', 'o', 'u']
test4 = consonants + vowels

letter = set(itertools.product(test4,repeat=n))

for l in letter:
    for j in range(len(l)):
        
        flag = 1
        if j != len(l) - 1:
            if l[j] in vowels and l[j+1] in vowels:
                flag = 0
                break
            if l[j] in consonants and l[j+1] in consonants:
                flag = 0
                break

    if flag:
        for j in l:
            sys.stdout.write(j)
        print ""
