#Node of a Singly Linked List
class Node:
	def __init__(self,data):
		self.data = None
		self.next = None

	def setData(self,data): #Setting the data field of the Node
		self.data = data

	def getData(self):   #Getting the data field of the Node
		return self.data

	def setNext(self,next):  #Setting the next field of the Node
		self.next = next

	def getNext(self):   #Getting the next field of the Node
		return self.next

	def hasNext(self):
		return self.next != None

#Traversing the linked list
def listLength(self):
	current = self.head
	count = 0
	while self.current != None:
		count += 1
		current = current.getNext()

#Inserting At Begining of a Node
def InsertBegining(self, data):
	newNode = Node()
	newNode.setData()
	if self.length == 0:
		self.head = newNode
	else:
		newNode.setNext(self.head)
		self.head = newNode
	self.length += 1